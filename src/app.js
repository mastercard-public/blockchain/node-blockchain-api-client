const inquirer = require('inquirer');
const blockchainService = require('./blockchain');
const protobuf = require('protobufjs');


async function run() {
  console.log();
  let answer = await promptCommand();
  processCommand(answer);
}

run();
const continuation = () => { run(); };

var lastBlock = '';
var timeout;

process.on('SIGINT', function() {
  clearInterval(timeout);
  continuation();
});


async function processCommand(command) {
  let operation = command.command;

  switch(operation) {
  case 'get status':
    console.log('Get Status...');
    blockchainService.getStatus(continuation);
    break;

  case 'get node info':
    console.log('Get Node Info...');
    blockchainService.getNodeInfo(null, continuation);
    break;

  case 'get block by hash or id':
    console.log('Get Block by Hash or ID...');
    let block = await promptBlock();
    blockchainService.getBlockById(block.id, continuation);
    break;

  case 'get last block':
    console.log('Get Last Block...');
    blockchainService.getLastBlock(continuation);
    break;

  case 'get block range':
    console.log('Get Block Range...');
    let params = await promptBlockRange();
    blockchainService.getBlockRange(params.offset, params.count, continuation);
    break;

  case 'create transactionentry':
    console.log('Create TransactionEntry...');
    let entry = await promptEntryValue();
    blockchainService.createEntry(Buffer.from(entry.value, 'utf8').toString('base64'), entry.partition, continuation);
    break;

  case 'create transactionentry with protobuf (pb02)':
    console.log('Create TransactionEntry with protobuf...');
    let entryProtobuf = await promptEntryValue();

    protobuf.load('src/pb02.proto', (err, root) => {
      if (err) {
        console.error(err);
      }

      let protobufMessage = root.lookupType('com.mastercard.blockchain.message.Message');
      let payload = {
        value: random(1, 1000000),
        otherValue: entryProtobuf.value,
        timestamp: Math.round(new Date() / 1000)
      };
      let errMsg = protobufMessage.verify(payload);
      if (errMsg) {
        console.error(errMsg);
      }
      let message = protobufMessage.create(payload);
      let buffer = protobufMessage.encode(message).finish();

      blockchainService.createEntry(buffer.toString('base64'), entryProtobuf.partition, continuation);
    });

    break;

  case 'get transactionentry':
    console.log('Get TransactionEntry...');
    let entryHash = await promptEntry();
    blockchainService.getEntry(entryHash.hash, continuation);
    break;

  case 'ticker':
    console.log('Ticker...');
    timeout = setInterval(() => {
      blockchainService.fetchLastBlock((data) => {
        if (lastBlock !== data[0].hash) {
          console.log(data[0]);
          lastBlock = data[0].hash;
        }
      });
    }, 500);
    break;

  default:
    exit();
  }
}


function promptCommand() {
  return inquirer.prompt({
    name: 'command',
    type: 'list',
    message: 'What do you want to do?',
    choices: [ 'Get Status',
               'Get Node Info',
               'Get Block by Hash or ID',
               'Get Last Block',
               'Get Block Range',
               'Create TransactionEntry',
               'Create TransactionEntry with Protobuf (PB02)',
               'Get TransactionEntry',
               'Ticker',
               new inquirer.Separator(),
               'Exit' ],
    filter: (val) => {
      return val.toLowerCase();
    }
  });
}

function promptNode() {
  return inquirer.prompt({
    name: 'node',
    type: 'string',
    message: 'What node?'
  });
}

function promptBlock() {
  return inquirer.prompt({
    name: 'id',
    type: 'string',
    message: 'What block?'
  });
}

function promptBlockRange() {
  return inquirer.prompt([
    {
      name: 'offset',
      type: 'integer',
      message: 'What offset?'
    },
    {
      name: 'count',
      type: 'integer',
      message: 'How many blocks?'
    }
  ]);
}

function promptEntryValue() {
  return inquirer.prompt([
    {
      name: 'value',
      type: 'string',
      message: 'What message?',
      default: 'Mastercard Blockchain rocks!'
    },
    {
      name: 'partition',
      type: 'string',
      message: 'What partition?',
      validate: (input) => {
        if (input && input.match(/^\w{4}$/g)) {
          return true;
        } else {
          return 'A Partition must have 4 alphanumeric characters.';
        }
      }
    }
  ]);
}

function promptEntry() {
  return inquirer.prompt({
    name: 'hash',
    type: 'string',
    message: 'What entry?'
  });
}

function random(low, high) {
  return Math.floor(Math.random() * (high - low) + low);
}

function exit() {
  console.log('Goodbye');
  process.exit(0);
}
