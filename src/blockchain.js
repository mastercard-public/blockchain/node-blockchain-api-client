const blockchain = require('mastercard-blockchain');
const MastercardAPI = blockchain.MasterCardAPI;

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

const authentication = new MastercardAPI.OAuth(
  config.consumer_key,
  config.key_store_path,
  config.key_alias,
  config.key_password
);

MastercardAPI.init({
  sandbox: true,
  debug: false,
  authentication: authentication
});

const blockchainServices = {

  // Gets the status of the Blockchain network.
  getStatus: function (callback) {
    var requestData = {};
    blockchain.Status.query(requestData, (error, data) => {
      if (!error) {
        console.log(data);
      }
      callback(data);
    });
  },

  // Get Info on the node.
  getNodeInfo: function(node, callback) {
    var requestData = {};
    if (node) {
      requestData.node = node;
    }
    blockchain.Node.query(requestData, (error, data) => {
      if (!error) {
        console.log(data);
      } else {
        console.error(`Error getting node info: ${error}`);
      }

      callback(data);
    });
  },

  getBlockById: function(id, callback) {
    blockchain.Block.read(id, {}, (error, data) => {
      if (!error) {
        console.log(data);
      } else {
        console.error(`Error getting block ${id}: ${error}`);
      }

      callback(data);
    });
  },

  fetchLastBlock: function(callback) {
    blockchain.Block.list({}, (error, data) => {
      if (error) {
        console.error(`Error getting block: ${error}`);
      }

      callback(data);
    });
  },

  getLastBlock: function(callback) {
    this.fetchLastBlock((data) => {
      console.log(data);
      callback(data);
    });
  },

  getBlockRange: function(from, count, callback) {
    blockchain.Block.list({
      offset: from,
      count: count
    }, (error, data) => {
      if (!error) {
        console.log(data);
      } else {
        console.error(`Error getting block range: ${error}`);
      }

      callback(data);
    });
  },

  // Create an entry into the blockchain; the value is assumed to be base64-encoded.
  createEntry(value, partition, callback) {
    blockchain.TransactionEntry.create({
      "app": partition,
      "encoding": 'base64',
      "value": value
    }, (error, data) => {
      if (!error) {
        console.log(data);
      } else {
        console.error(`Error creating entry: ${error}`);
      }

      callback(data);
    });
  },

  getEntry(hash, callback) {
    blockchain.TransactionEntry.read("", {
      "hash": hash
    }, (error, data) => {
      if (!error) {
        console.log(data);
        console.log();
        console.log(`Message: ${Buffer.from(data.value, 'hex').toString('utf8')}`);
      } else {
        console.error(`Error getting entry ${hash}: ${error}`);
      }

      callback(data);
    });
  },

};

module.exports = blockchainServices;
