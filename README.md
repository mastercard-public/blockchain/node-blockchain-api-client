# Mastercard Blockchain Client

A simple console application which demonstrates how to use the
Mastercard Blockchain. For details, see the SDK reference
documentation at
https://developer.mastercard.com/documentation/blockchain/1


## Getting Started

### Pre-Requisites

  * node.js v10.5.0+
  * A developer account on https://developer.mastercard.com and a
    developer project for Mastercard Blockchain

### Configure the application

Once you have accessed Mastercard Developers and created a Blockchain
project, you should have got:

* A p12 file,
* A consumer key for which you have set the password and the alias of
  the entry holding the private key.

Edit `config.json` to set:

* Your consumer key,
* The location of the p12 file,
* The password of the p12 store,
* The alias of the key entry.

### Install the dependencies

```
npm install
```

### Run the Project

```
node src/app.js
```

You can also use the following `npm` script:

```
npm run menu
```


You should get the following meny:

```
? What do you want to do? (Use arrow keys)
❯ Get Status
  Get Node Info
  Get Block by Hash or ID
  Get Last Block
  Get Block Range
  Create TransactionEntry
  Create TransactionEntry with Protobuf (PB02)
(Move up and down to reveal more choices)
```

You can use the arrow keys to navigate the menu, and enter to run a
command.
